﻿using DotNet.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace DotNet.Services
{
    public class BillDetailService
    {
        private readonly IMongoCollection<BillDetail> _billdetails;
        public BillDetailService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("Connection"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _billdetails = database.GetCollection<BillDetail>("bill_details");
        }
        public BillDetail Get(string id)
        {
            return _billdetails.Find<BillDetail>(i => i.Id == id).FirstOrDefault();
        }

        public List<BillDetail> Get(string[] ids)
        {
            return _billdetails.Find<BillDetail>(i => ids.Contains(i.Id)).ToList();
        }

        public List<BillDetail> List(string sort = "", string filter = "{}")
        {
            var builder = Builders<BillDetail>.Filter;
            FilterDefinition<BillDetail> query = new ExpressionFilterDefinition<BillDetail>(i=> true);
            if (filter != null)
            {
                var resource = JObject.Parse(filter);
                foreach (var property in resource)
                {
                    query = query & builder.Regex(property.Key, property.Value.ToString());
                }
            }
            return _billdetails.Find(query).Sort(sort).ToList();
        }
 
        public BillDetail Create(BillDetail i)
        {
            _billdetails.InsertOne(i);
            return i;
        }

        public void Update(string id, BillDetail input)
        {
            _billdetails.ReplaceOne(i => i.Id == id, input);
        }

        public void Remove(string id)
        {
            _billdetails.DeleteOne(i => i.Id == id);
        }

        public void Remove(string[] ids)
        {
            _billdetails.DeleteMany(i => ids.Contains(i.Id));
        }
    }
}
