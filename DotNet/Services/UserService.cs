﻿using DotNet.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace DotNet.Services
{
    public class UserService
    {
        private readonly IMongoCollection<Users> _user;
        private readonly IMongoCollection<Tokens> _token;
        public IConfiguration Config{ get; }
        public UserService(IConfiguration config)
        {
            Config = config;
            var client = new MongoClient(config.GetConnectionString("Connection"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _user = database.GetCollection<Users>("users");
            _token = database.GetCollection<Tokens>("tokens");
        }
        public Users Get(string id)
        {
            return _user.Find<Users>(user => user.Id == id).FirstOrDefault();
        }
        public Users GetByEmail(string email)
        {
            return _user.Find(user => user.Email == email).FirstOrDefault();
        }
        public Users GetByUsername(string username)
        {
            return _user.Find(user => user.Username == username).FirstOrDefault();
        }

        public List<Users> Get(string[] ids)
        {
            return _user.Find<Users>(user => ids.Contains(user.Id)).ToList();
        }

        public List<Users> List(string sort = "", string filter = "{}")
        {
            var builder = Builders<Users>.Filter;
            FilterDefinition<Users> query = new ExpressionFilterDefinition<Users>(i=> true);
            if (filter != null)
            {
                var resource = JObject.Parse(filter);
                foreach (var property in resource)
                {
                    query = query & builder.Regex(property.Key, property.Value.ToString());
                }
            }
            return _user.Find(query).Sort(sort).ToList();
        }
 
        public Users Create(Users item)
        {
            _user.InsertOne(item);
            return item;
        }

        public void Update(string id, Users userIn)
        {
            _user.ReplaceOne(user => user.Id == id, userIn);
        }

        public void Remove(string id)
        {
            _user.DeleteOne(user => user.Id == id);
        }

        public void Remove(string[] ids)
        {
            _user.DeleteMany(user => ids.Contains(user.Id));
        }
        
        public Users CheckAuth(string username, string password)
        {
            return _user.Find(user => user.Username == username && user.Password == password).FirstOrDefault();
        }
        public Tokens CreateToken(Tokens item)
        {
            _token.InsertOne(item);
            return item;
        }
    }
}
