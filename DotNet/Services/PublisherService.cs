﻿using DotNet.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace DotNet.Services
{
    public class PublisherService
    {
        private readonly IMongoCollection<Publishers> _publisher;
        public PublisherService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("Connection"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _publisher = database.GetCollection<Publishers>("publishers");
        }
        public Publishers Get(string id)
        {
            return _publisher.Find<Publishers>(i => i.Id == id).FirstOrDefault();
        }

        public List<Publishers> Get(string[] ids)
        {
            return _publisher.Find<Publishers>(i => ids.Contains(i.Id)).ToList();
        }

        public List<Publishers> List(string sort = "", string filter = "{}")
        {
            var builder = Builders<Publishers>.Filter;
            FilterDefinition<Publishers> query = new ExpressionFilterDefinition<Publishers>(i=> true);
            if (filter != null)
            {
                var resource = JObject.Parse(filter);
                foreach (var property in resource)
                {
                    query = query & builder.Regex(property.Key, property.Value.ToString());
                }
            }
            return _publisher.Find(query).Sort(sort).ToList();
        }
 
        public Publishers Create(Publishers i)
        {
            _publisher.InsertOne(i);
            return i;
        }

        public void Update(string id, Publishers input)
        {
            _publisher.ReplaceOne(i => i.Id == id, input);
        }

        public void Remove(string id)
        {
            _publisher.DeleteOne(i => i.Id == id);
        }

        public void Remove(string[] ids)
        {
            _publisher.DeleteMany(i => ids.Contains(i.Id));
        }
    }
}
