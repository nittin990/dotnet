﻿using DotNet.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace DotNet.Services
{
    public class BillService
    {
        private readonly IMongoCollection<Bills> _bill;
        public IConfiguration Config{ get; }
        public BillService(IConfiguration config)
        {
            Config = config;
            var client = new MongoClient(config.GetConnectionString("Connection"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _bill = database.GetCollection<Bills>("bills");
        }
        public Bills Get(string id)
        {
            return _bill.Find<Bills>(i => i.Id == id).FirstOrDefault();
        }

        public List<Bills> Get(string[] ids)
        {
            return _bill.Find<Bills>(i => ids.Contains(i.Id)).ToList();
        }

        public List<Bills> List(string sort = "", string filter = "{}")
        {
            var builder = Builders<Bills>.Filter;
            FilterDefinition<Bills> query = new ExpressionFilterDefinition<Bills>(i=> true);
            if (filter != null)
            {
                var resource = JObject.Parse(filter);
                foreach (var property in resource)
                {
                    query = query & builder.Regex(property.Key, property.Value.ToString());
                }
            }
            return _bill.Find(query).Sort(sort).ToList();
        }
 
        public Bills Create(Bills i)
        {
            _bill.InsertOne(i);
            return i;
        }

        public void Update(string id, Bills input)
        {
            _bill.ReplaceOne(i => i.Id == id, input);
        }

        public void Remove(string id)
        {
            _bill.DeleteOne(i => i.Id == id);
        }

        public void Remove(string[] ids)
        {
            _bill.DeleteMany(i => ids.Contains(i.Id));
        }
    }
}
