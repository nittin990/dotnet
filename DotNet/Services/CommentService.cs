﻿using DotNet.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace DotNet.Services
{
    public class CommentService
    {
        private readonly IMongoCollection<Comments> _comment;
        public CommentService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("Connection"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _comment = database.GetCollection<Comments>("comments");
        }
        public Comments Get(string id)
        {
            return _comment.Find<Comments>(i => i.Id == id).FirstOrDefault();
        }

        public List<Comments> Get(string[] ids)
        {
            return _comment.Find<Comments>(i => ids.Contains(i.Id)).ToList();
        }

        public List<Comments> List(string sort = "", string filter = "{}")
        {
            var builder = Builders<Comments>.Filter;
            FilterDefinition<Comments> query = new ExpressionFilterDefinition<Comments>(i=> true);
            if (filter != null)
            {
                var resource = JObject.Parse(filter);
                foreach (var property in resource)
                {
                    query = query & builder.Regex(property.Key, property.Value.ToString());
                }
            }
            return _comment.Find(query).Sort(sort).ToList();
        }
 
        public Comments Create(Comments i)
        {
            _comment.InsertOne(i);
            return i;
        }

        public void Update(string id, Comments input)
        {
            _comment.ReplaceOne(i => i.Id == id, input);
        }

        public void Remove(string id)
        {
            _comment.DeleteOne(i => i.Id == id);
        }

        public void Remove(string[] ids)
        {
            _comment.DeleteMany(i => ids.Contains(i.Id));
        }
    }
}
