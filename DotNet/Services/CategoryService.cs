﻿using DotNet.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace DotNet.Services
{
    public class CategoryService
    {
        private readonly IMongoCollection<Categories> _category;
        public CategoryService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("Connection"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _category = database.GetCollection<Categories>("categories");
        }
        public Categories Get(string id)
        {
            return _category.Find(i => i.Id == id).FirstOrDefault();
        }

        public List<Categories> Get(string[] ids)
        {
            return _category.Find(i => ids.Contains(i.Id)).ToList();
        }

        public List<Categories> List(string sort = "", string filter = "{}")
        {
            var builder = Builders<Categories>.Filter;
            FilterDefinition<Categories> query = new ExpressionFilterDefinition<Categories>(i=> true);
            if (filter != null)
            {
                var resource = JObject.Parse(filter);
                foreach (var property in resource)
                {
                    query = query & builder.Regex(property.Key, property.Value.ToString());
                }
            }
            return _category.Find(query).Sort(sort).ToList();
        }
 
        public Categories Create(Categories i)
        {
            _category.InsertOne(i);
            return i;
        }

        public void Update(string id, Categories input)
        {
            _category.ReplaceOne(i => i.Id == id, input);
        }

        public void Remove(string id)
        {
            _category.DeleteOne(i => i.Id == id);
        }

        public void Remove(string[] ids)
        {
            _category.DeleteMany(i => ids.Contains(i.Id));
        }
    }
}
