﻿using DotNet.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace DotNet.Services
{
    public class ProductService
    {
        private readonly IMongoCollection<Products> _product;
        public ProductService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("Connection"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _product = database.GetCollection<Products>("products");
        }
        public List<Products> GetNewProduct()
        {
            return _product.Find(product => product.TopSells == 1).Limit(4).ToList();
        }
        public List<Products> GetByCategory(string categoryId)
        {
            return _product.Find(product => product.Category_id == categoryId).Limit(20).ToList();
        }
        
        public List<Products> GetByPublisher(string publisherId)
        {
            return _product.Find(product => product.Publisher_id == publisherId).Limit(3).ToList();
        }
        
        public Products Get(string id)
        {
            return _product.Find(i => i.Id == id).FirstOrDefault();
        }

        public List<Products> Get(string[] ids)
        {
            return _product.Find(i => ids.Contains(i.Id)).ToList();
        }

        public List<Products> List(string sort = "", string filter = "{}")
        {
            var builder = Builders<Products>.Filter;
            FilterDefinition<Products> query = new ExpressionFilterDefinition<Products>(i=> true);
            if (filter != null)
            {
                var resource = JObject.Parse(filter);
                foreach (var property in resource)
                {
                    query = query & builder.Regex(property.Key, property.Value.ToString());
                }
            }
            return _product.Find(query).Sort(sort).ToList();
        }
 
        public Products Create(Products i)
        {
            _product.InsertOne(i);
            return i;
        }

        public void Update(string id, Products product)
        {
            _product.ReplaceOne(i => i.Id == id, product);
        }

        public void Remove(string id)
        {
            _product.DeleteOne(i => i.Id == id);
        }

        public void Remove(string[] ids)
        {
            _product.DeleteMany(i => ids.Contains(i.Id));
        }
    }
}
