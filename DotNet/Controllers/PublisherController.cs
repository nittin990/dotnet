﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNet.Models;
using DotNet.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotNet.Controllers
{
    [Route("api/publisher")]
    public class PublisherController : Controller
    {
        private readonly PublisherService _publisher;
        public PublisherController(PublisherService publisher)
        {
            _publisher = publisher;
        }
        // GET: api/<controller>
        [HttpGet]
        public ActionResult<List<Publishers>> Get([FromQuery]string[] id, [FromQuery]int page, [FromQuery]int size, [FromQuery]string sort, [FromQuery]string filter)
        {
            var list = id.Length == 0 ? _publisher.List(sort, filter) : _publisher.Get(id);
            Response.Headers.Add("X-Total-Count", list.Count.ToString());
            // No paging
            if (page == 0 && size == 0)
            {
                return list;
            }
            // Has paging
            return list.Skip(page * size).Take(size).ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetPublisher")]
        public ActionResult<Publishers> Get(string id)
        {
            var item = _publisher.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        // POST api/<controller>
        [HttpPost]
        public ActionResult<Publishers> Create([FromBody]Publishers publisherIn)
        {
            _publisher.Create(publisherIn);
            return CreatedAtRoute("GetPublisher", new { id = publisherIn.Id }, publisherIn);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public ActionResult<Publishers> Update(string id, [FromBody]Publishers itemIn)
        {
            var item = _publisher.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            _publisher.Update(id, itemIn);
            return itemIn;
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var publisher = _publisher.Get(id);
            if (publisher == null)
            {
                return NotFound();
            }
            _publisher.Remove(publisher.Id);
            return NoContent();
        }

        // DELETE api/<controller>
        [HttpDelete]
        public ActionResult<List<Publishers>> Delete([FromQuery]string[] id)
        {
            var publishers = _publisher.Get(id);
            if (publishers.Count != id.Length)
            {
                return NotFound();
            }
            _publisher.Remove(id);
            return publishers;
        }
    }
}
