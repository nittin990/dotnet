﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNet.Models;
using DotNet.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotNet.Controllers
{
    [Route("api/product")]
    public class ProductController : Controller
    {
        private readonly ProductService _product;
        public ProductController(ProductService product)
        {
            _product = product;
        }
        // GET: api/<controller>
        [HttpGet]
        public ActionResult<List<Products>> Get([FromQuery]string[] id, [FromQuery]int page, [FromQuery]int size, [FromQuery]string sort, [FromQuery]string filter)
        {
            var list = id.Length == 0 ? _product.List(sort, filter) : _product.Get(id);
            Response.Headers.Add("X-Total-Count", list.Count.ToString());
            // No paging
            if (page == 0 && size == 0)
            {
                return list;
            }
            // Has paging
            return list.Skip(page * size).Take(size).ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetProduct")]
        public ActionResult<Products> Get(string id)
        {
            var product = _product.Get(id);
            if (product == null)
            {
                return NotFound();
            }
            return product;
        }

        // POST api/<controller>
        [HttpPost]
        public ActionResult<Products> Create([FromBody]Products productIn)
        {
            _product.Create(productIn);
            return CreatedAtRoute("GetProduct", new { id = productIn.Id }, productIn);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public ActionResult<Products> Update(string id, [FromBody]Products productIn)
        {
            var product = _product.Get(id);
            if (product == null)
            {
                return NotFound();
            }
            _product.Update(id, productIn);
            return productIn;
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var product = _product.Get(id);
            if (product == null)
            {
                return NotFound();
            }
            _product.Remove(product.Id);
            return NoContent();
        }

        // DELETE api/<controller>
        [HttpDelete]
        public ActionResult<List<Products>> Delete([FromQuery]string[] id)
        {
            var products = _product.Get(id);
            if (products.Count != id.Length)
            {
                return NotFound();
            }
            _product.Remove(id);
            return products;
        }
        
        
        // MORE API
        
        // GET api/<controller>/
        //Get new product
        [HttpGet("new")]
        public ActionResult<List<Products>> GetNew()
        {
            return _product.GetNewProduct();
        }
        // GET api/<controller>/
        //Get products by publisher
        [HttpGet("publisher/{id}")]
        public ActionResult<List<Products>> GetByPublisher(string id)
        {
            return _product.GetByPublisher(id);
        }
        // GET api/<controller>/
        //Get products by category
        [HttpGet("category/{id}")]
        public ActionResult<List<Products>> GetByCategory(string id)
        {
            return _product.GetByCategory(id);
        }
    }
}
