﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DotNet.Models;
using DotNet.Services;
using Google.Apis.Oauth2.v2;
using Google.Apis.Services;
using Newtonsoft.Json.Linq;
using PusherServer;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotNet.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly UserService _user;
        private readonly Pusher _pusher;
        public UserController(UserService user)
        {
            _user = user;
            var config = _user.Config;
            var options = new PusherOptions { Cluster = config["Pusher:Cluster"], Encrypted = true };
            _pusher = new Pusher( config["Pusher:AppId"], config["Pusher:AppKey"], config["Pusher:AppSecret"], options);
       
        }
        // GET: api/user
        [HttpGet]
        public ActionResult<List<Users>> Get([FromQuery]string[] id, [FromQuery]int page, [FromQuery]int size, [FromQuery]string sort, [FromQuery]string filter)
        {
            var list = id.Length == 0 ? _user.List(sort, filter) : _user.Get(id);
            Response.Headers.Add("X-Total-Count", list.Count.ToString());
            // No paging
            if (page == 0 && size == 0)
            {
                return list;
            }
            // Has paging
            return list.Skip(page * size).Take(size).ToList();
        }

        // GET api/user/5
        [HttpGet("{id}", Name = "GetUser")]
        public ActionResult<Users> Get(string id)
        {
            var item = _user.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        // POST api/user
        [HttpPost]
        public ActionResult<Users> Create([FromBody]Users input)
        {
            _user.Create(input);
            return CreatedAtRoute("GetUser", new { id = input.Id }, input);
        }

        // PUT api/user/5
        [HttpPut("{id}")]
        public ActionResult<Users> Update(string id, [FromBody]Users input)
        {
            var item = _user.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            _user.Update(id, input);
            return input;
        }

        // DELETE api/user/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var item = _user.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            _user.Remove(item.Id);
            return NoContent();
        }

        // DELETE api/user
        [HttpDelete]
        public ActionResult<List<Users>> Delete([FromQuery]string[] id)
        {
            var items = _user.Get(id);
            if (items.Count != id.Length)
            {
                return NotFound();
            }
            _user.Remove(id);
            return items;
        }

        private string GetTokenString(string id, int dateExpire)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var secret = _user.Config["Secret:Internal"];
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] 
                {
                    new Claim(ClaimTypes.Name, id)
                }),
                Expires = DateTime.UtcNow.AddDays(dateExpire),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }
        // POST api/user/login
        [HttpPost("login")]
        public ActionResult<Tokens> Login([FromBody]Users form)
        {
            var user = _user.CheckAuth(form.Username, form.Password);
            if (user == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            var tokenString = GetTokenString(user.Id, 7);
            return Ok(new {
                user.Id,
                user.Username,
                user.Name,
                user.Photo,
                Token = tokenString
            });
        }

        // POST api/user/sign-up
        [HttpPost("sign-up")]
        public ActionResult<Tokens> SignUp([FromBody]Users form)
        {
            var checkUsername = _user.GetByUsername(form.Username);
            var checkEmail = _user.GetByEmail(form.Email);
            if (checkUsername != null)
            {
                return BadRequest(new { message = "Username is existed" });
            }
            if (form.Email != "" && checkEmail != null)
            {
                return BadRequest(new { message = "Email is existed" });
            }
            _user.Create(form);
            var tokenString = GetTokenString(form.Id, 7);
            return Ok(new {
                form.Id,
                form.Username,
                form.Name,
                form.Photo,
                Token = tokenString
            });
        }

        // GET api/user/profile
        [HttpGet("profile", Name = "GetMyProfile")]
        [Authorize]
        public async Task<ActionResult<Users>> GetProfile()
        {
            var userId = User.FindFirst(ClaimTypes.Name).Value;
            var item = _user.Get(userId);
            if (item == null)
            {
                return NotFound();
            }
            _user.CreateToken(new Tokens{ Channel = "authorized", User_id = item.Id, Expire = DateTime.Now});
            if (item.Role_id != 1)
            {
                await _pusher.TriggerAsync( "admin", "userAttempt", item);
            }
            return item;
        }
        
        // GET api/user/google
        [HttpPost("google/{token}")]
        public ActionResult<Users> LoginGoogle(string token, [FromBody]Users input)
        {
            var googleInitializer = new BaseClientService.Initializer();
            googleInitializer.ApiKey = _user.Config["Google:ClientSecret"];
            var service = new Oauth2Service(googleInitializer);
            var request = service.Tokeninfo();
            request.AccessToken = token;
            try
            {
                var googleUser = request.ExecuteAsync().Result;
                var user = _user.GetByUsername(googleUser.UserId);
                if (user == null)
                {
                    _user.Create(input);
                    user = input;
                }
                var tokenString = GetTokenString(user.Id, 7);
                return Ok(new {
                    user.Id,
                    user.Username,
                    user.Name,
                    user.Photo,
                    Token = tokenString
                });
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine(e);
                return NotFound();
            }
        }
        // GET api/user/google
        [HttpPost("facebook/{token}")]
        public async Task<ActionResult<Users>> LoginFacebook(string token, [FromBody]Users input)
        {
            var httpClient = new HttpClient { BaseAddress = new Uri("https://graph.facebook.com/v2.9/") };
            var response = httpClient.GetAsync($"me?access_token={token}&fields=id,name,email");
            try
            {
                var content = await response.Result.Content.ReadAsStringAsync();
                var result = JObject.Parse(content);
                var user = _user.GetByUsername(result["id"].ToString());
                if (user == null)
                {
                    input.Email = result["email"].ToString();
                    _user.Create(input);
                    user = input;
                }
                var tokenString = GetTokenString(user.Id, 7);
                return Ok(new {
                    user.Id,
                    user.Username,
                    user.Name,
                    user.Photo,
                    Token = tokenString
                });
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }
    }
}
