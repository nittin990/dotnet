﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DotNet.Models;
using DotNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PusherServer;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotNet.Controllers
{
    [Route("api/bill")]
    public class BillsController : Controller
    {
        private readonly BillService _bill;
        private readonly BillDetailService _billdetail;
        private readonly Pusher _pusher;
        public BillsController(BillService bill, BillDetailService billdetail)
        {
            _bill = bill;
            _billdetail = billdetail;
            var config = _bill.Config;
            var options = new PusherOptions { Cluster = config["Pusher:Cluster"], Encrypted = true };
            _pusher = new Pusher( config["Pusher:AppId"], config["Pusher:AppKey"], config["Pusher:AppSecret"], options);

        }
        // GET: api/<controller>
        [HttpGet]
        public ActionResult<List<Bills>> Get([FromQuery]string[] id, [FromQuery]int page, [FromQuery]int size, [FromQuery]string sort, [FromQuery]string filter)
        {
            var list = id.Length == 0 ? _bill.List(sort, filter) : _bill.Get(id);
            Response.Headers.Add("X-Total-Count", list.Count.ToString());
            // No paging
            if (page == 0 && size == 0)
            {
                return list;
            }
            // Has paging
            return list.Skip(page * size).Take(size).ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetBill")]
        public ActionResult<Bills> Get(string id)
        {
            var item = _bill.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        // POST api/<controller>
        [HttpPost]
        public ActionResult<Bills> Create([FromBody]Bills input)
        {
            _bill.Create(input);
            return CreatedAtRoute("GetBill", new { id = input.Id }, input);
        }

        [HttpPost("checkOut")]
        public async Task<ActionResult<Bills>> CreateCheckOutBill([FromBody]Bills input)
        {
            _bill.Create(input);
            
            await _pusher.TriggerAsync( "admin", "checkOut", input );
            
            return input;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Bills>> Update(string id, [FromBody]Bills input)
        {
            var item = _bill.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            _bill.Update(id, input);

            if (item.Status_of_bill != input.Status_of_bill &&
                (input.Status_of_bill == 1 || input.Status_of_bill == 2))
            {
                var eventName = input.Status_of_bill == 1 ? "billApproved" : "billRejected";
                await _pusher.TriggerAsync( input.User_id, eventName,  input);
            }

            return input;
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var item = _bill.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            _bill.Remove(item.Id);
            return NoContent();
        }

        // DELETE api/<controller>
        [HttpDelete]
        public ActionResult<List<Bills>> Delete([FromQuery]string[] id)
        {
            var items = _bill.Get(id);
            if (items.Count != id.Length)
            {
                return NotFound();
            }
            _bill.Remove(id);
            return items;
        }
        
        // GET api/<controller>/5
        [HttpGet("my-bills")]
        [Authorize]
        public ActionResult<List<Bills>> GetMyBills([FromQuery]int page, [FromQuery]int size)
        {
            var userId = User.FindFirst(ClaimTypes.Name).Value;
            return Get(new string[]{}, page, size, "{\"date_of_bill\":-1}", $"{{\"user_id\":\"{userId}\"}}");
        }
    }
}
