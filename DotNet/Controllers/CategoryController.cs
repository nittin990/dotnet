﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNet.Models;
using DotNet.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotNet.Controllers
{
    [Route("api/category")]
    public class CategoryController : Controller
    {
        private readonly CategoryService _category;
        public CategoryController(CategoryService category)
        {
            _category = category;
        }
           // GET: api/<controller>
        [HttpGet]
        public ActionResult<List<Categories>> Get([FromQuery]string[] id, [FromQuery]int page, [FromQuery]int size, [FromQuery]string sort, [FromQuery]string filter)
        {
            var list = id.Length == 0 ? _category.List(sort, filter) : _category.Get(id);
            Response.Headers.Add("X-Total-Count", list.Count.ToString());
            // No paging
            if (page == 0 && size == 0)
            {
                return list;
            }
            // Has paging
            return list.Skip(page * size).Take(size).ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetCategory")]
        public ActionResult<Categories> Get(string id)
        {
            var item = _category.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        // POST api/<controller>
        [HttpPost]
        public ActionResult<Categories> Create([FromBody]Categories itemIn)
        {
            _category.Create(itemIn);
            return CreatedAtRoute("GetCategory", new { id = itemIn.Id }, itemIn);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public ActionResult<Categories> Update(string id, [FromBody]Categories itemIn)
        {
            var item = _category.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            _category.Update(id, itemIn);
            return itemIn;
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var item = _category.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            _category.Remove(item.Id);
            return NoContent();
        }

        // DELETE api/<controller>
        [HttpDelete]
        public ActionResult<List<Categories>> Delete([FromQuery]string[] id)
        {
            var items = _category.Get(id);
            if (items.Count != id.Length)
            {
                return NotFound();
            }
            _category.Remove(id);
            return items;
        }
    }
}
