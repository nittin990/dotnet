﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DotNet.Models;
using DotNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotNet.Controllers
{
    [Route("api/comment")]
    public class CommentController : Controller
    {
        private readonly CommentService _comment;
        public CommentController(CommentService comment)
        {
            _comment = comment;
        }
        // GET: api/comment
        [HttpGet]
        public ActionResult<List<Comments>> Get([FromQuery]string[] id, [FromQuery]int page, [FromQuery]int size, [FromQuery]string sort, [FromQuery]string filter)
        {
            var list = id.Length == 0 ? _comment.List(sort, filter) : _comment.Get(id);
            Response.Headers.Add("X-Total-Count", list.Count.ToString());
            // No paging
            if (page == 0 && size == 0)
            {
                return list;
            }
            // Has paging
            return list.Skip(page * size).Take(size).ToList();
        }

        // GET api/comment/5
        [HttpGet("{id}", Name = "GetComment")]
        public ActionResult<Comments> Get(string id)
        {
            var item = _comment.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        // POST api/comment
        [HttpPost]
        public ActionResult<Comments> Create([FromBody]Comments input)
        {
            _comment.Create(input);
            return CreatedAtRoute("GetComment", new { id = input.Id }, input);
        }

        // PUT api/comment/5
        [HttpPut("{id}")]
        public ActionResult<Comments> Update(string id, [FromBody]Comments itemIn)
        {
            var item = _comment.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            _comment.Update(id, itemIn);
            return itemIn;
        }

        // DELETE api/comment/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var publisher = _comment.Get(id);
            if (publisher == null)
            {
                return NotFound();
            }
            _comment.Remove(publisher.Id);
            return NoContent();
        }

        // DELETE api/comment
        [HttpDelete]
        public ActionResult<List<Comments>> Delete([FromQuery]string[] id)
        {
            var comments = _comment.Get(id);
            if (comments.Count != id.Length)
            {
                return NotFound();
            }
            _comment.Remove(id);
            return comments;
        }
        
        // GET: api/comment/product/{id}
        [HttpGet("product/{id}")]
        public ActionResult<List<Comments>> GetByProduct(string id, [FromQuery]int page, [FromQuery]int size)
        {
            return Get(new string[]{}, page, size, "{\"date\":1}", $"{{\"product_id\":\"{id}\"}}");
        }
        // POST api/comment/product/{id}
        [HttpPost("product/{id}")]
        [Authorize]
        public ActionResult<Comments> Create(string id, [FromBody]Comments input)
        {
            var userId = User.FindFirst(ClaimTypes.Name).Value;
            input.Product_id = id;
            input.User_id = userId;
            input.Date = DateTime.Now;
            _comment.Create(input);
            return CreatedAtRoute("GetComment", new { id = input.Id }, input);
        }
        // DELETE api/comment
        [HttpDelete("me/{id}")]
        [Authorize]
        public ActionResult<Comments> DeleteByMe(string id)
        {
            var comments = _comment.Get(id);
            var userId = User.FindFirst(ClaimTypes.Name).Value;
            if (comments == null || comments.User_id != userId)
            {
                return NotFound();
            }
            _comment.Remove(id);
            return comments;
        }
    }
}
