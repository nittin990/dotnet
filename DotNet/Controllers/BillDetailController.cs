﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNet.Models;
using DotNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotNet.Controllers
{
    [Route("api/billdetail")]
    public class BillDetailController : Controller
    {
        private readonly BillDetailService _billdetails;
        public BillDetailController(BillDetailService billdetails)
        {
            _billdetails = billdetails;
        }
        // GET: api/<controller>
        [HttpGet]
        public ActionResult<List<BillDetail>> Get([FromQuery]string[] id, [FromQuery]int page, [FromQuery]int size, [FromQuery]string sort, [FromQuery]string filter)
        {
            var list = id.Length == 0 ? _billdetails.List(sort, filter) : _billdetails.Get(id);
            Response.Headers.Add("X-Total-Count", list.Count.ToString());
            // No paging
            if (page == 0 && size == 0)
            {
                return list;
            }
            // Has paging
            return list.Skip(page * size).Take(size).ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetBillDetail")]
        public ActionResult<BillDetail> Get(string id)
        {
            var item = _billdetails.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        // POST api/<controller>
        [HttpPost]
        public ActionResult<BillDetail> Create([FromBody]BillDetail publisherIn)
        {
            _billdetails.Create(publisherIn);
            return CreatedAtRoute("GetBillDetail", new { id = publisherIn.Id }, publisherIn);
        }

        [HttpPost("checkOut")]
        public ActionResult<BillDetail> CreateCheckOutBillDetail([FromBody]BillDetail input)
        {
            _billdetails.Create(input);
            return input;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public ActionResult<BillDetail> Update(string id, [FromBody]BillDetail itemIn)
        {
            var item = _billdetails.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            _billdetails.Update(id, itemIn);
            return itemIn;
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var publisher = _billdetails.Get(id);
            if (publisher == null)
            {
                return NotFound();
            }
            _billdetails.Remove(publisher.Id);
            return NoContent();
        }

        // DELETE api/<controller>
        [HttpDelete]
        public ActionResult<List<BillDetail>> Delete([FromQuery]string[] id)
        {
            var publishers = _billdetails.Get(id);
            if (publishers.Count != id.Length)
            {
                return NotFound();
            }
            _billdetails.Remove(id);
            return publishers;
        }
        
        // GET: api/<controller>
        [HttpGet("detail-of/{id}")]
        [Authorize]
        public ActionResult<List<BillDetail>> Get(string id, [FromQuery]int page, [FromQuery]int size)
        {
            return Get(new string[]{}, page, size, "{\"_id\":-1}", $"{{\"bill_id\":\"{id}\"}}");
        }
    }
}
