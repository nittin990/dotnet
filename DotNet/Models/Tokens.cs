using System;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DotNet.Models
{
    [BsonIgnoreExtraElements]
    public class Tokens
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("user_id")]
        public string User_id { get; set; }

        [BsonElement("channel")]
        public string Channel { get; set; }

        [BsonElement("expire")]
        public DateTime Expire { get; set; }
    }
}