﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNet.Models
{
    [BsonIgnoreExtraElements]
    public class Products
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("product_name")]
        public string Product_name { get; set; }

        [BsonElement("price")]
        public decimal Price { get; set; }

        [BsonElement("publisher_id")]
        public string Publisher_id { get; set; }

        [BsonElement("category_id")]
        public string Category_id { get; set; }

        [BsonElement("technical_parameter")]
        public string Technical_parameter { get; set; }

        [BsonElement("product_image")]
        public string Product_image { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("top_sells")]
        public int TopSells { get; set; }
    }
}
