﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNet.Models
{
    [BsonIgnoreExtraElements]
    public class Comments
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("detail")]
        public string Detail { get; set; }

        [BsonElement("product_id")]
        public string Product_id { get; set; }

        [BsonElement("user_id")]
        public string User_id { get; set; }

        [BsonElement("date")]
        public DateTime Date { get; set; }
    }
}
