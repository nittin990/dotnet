﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNet.Models
{
    [BsonIgnoreExtraElements]
    public class BillDetail
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("bill_id")]
        public string Bill_id { get; set; }

        [BsonElement("product_id")]
        public string Product_id { get; set; }

        [BsonElement("quantity")]
        public int Quantity { get; set; }

        [BsonElement("total_price")]
        public decimal Total_price { get; set; }
    }
}
