﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNet.Models
{
    [BsonIgnoreExtraElements]
    public class Bills
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name_reciever")]
        public string Name_reciever { get; set; }

        [BsonElement("phone_number")]
        public string Phone_number { get; set; }

        [BsonElement("address")]
        public string Address { get; set; }

        [BsonElement("note")]
        public string Note { get; set; }

        [BsonElement("user_id")]
        public string User_id { get; set; }

        [BsonElement("status_of_bill")]
        public int Status_of_bill { get; set; }

        [BsonElement("total")]
        public decimal Total { get; set; }

        [BsonElement("date_of_bill")]
        public DateTime Date_of_bill { get; set; }
    }
}
